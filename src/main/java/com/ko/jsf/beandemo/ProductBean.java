package com.ko.jsf.beandemo;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class ProductBean {

	// field
	private String kindOfProduct;

	// no-arg constructor
	public ProductBean() {

	}

	// getter and setter
	public String getKindOfProduct() {
		return kindOfProduct;
	}

	public void setKindOfProduct(String kindOfProduct) {
		this.kindOfProduct = kindOfProduct;
	}

	public String showTheProducts() {
		if (kindOfProduct != null && kindOfProduct.equals("clothes")) {
			return "clothes_products";
		} else {
			return "tea_products";
		}
	}

}
